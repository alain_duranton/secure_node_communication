%% @author alain.duranton
%% @doc @todo Add description to cipher_tools_tests.


-module(cipher_tools_tests).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").
-endif.
%% ====================================================================
%% API functions
%% ====================================================================
-export([]).



%% ====================================================================
%% Internal functions
%% ====================================================================


-ifdef(TEST).
crypto_test_() ->    
    {PublicKeyAlice, PrivateKeyAlice} = cipher_tools:generate_key_pair(),
    {PublicKeyBob, PrivateKeyBob} = cipher_tools:generate_key_pair(),
    {PublicKeyEve, PrivateKeyEve} = cipher_tools:generate_key_pair(),
    [
     ?_assertEqual(
       <<>>,
       cipher_tools:decipher(
         cipher_tools:cipher(<<"FUBAR">>,PublicKeyBob,PrivateKeyAlice),
         PublicKeyAlice,PrivateKeyEve))
     , ?_assertEqual(
       <<"FUBAR">>,
       cipher_tools:decipher(
         cipher_tools:cipher(<<"FUBAR">>,PublicKeyBob,PrivateKeyAlice),
         PublicKeyAlice,PrivateKeyBob))
     , ?_assertEqual(
       <<>>,
       cipher_tools:decipher(
         cipher_tools:cipher(<<"foobar">>,PublicKeyEve,PrivateKeyAlice),
         PublicKeyAlice,PrivateKeyBob))
     , ?_assertEqual(
       <<"foobar">>,
       cipher_tools:decipher(
         cipher_tools:cipher(<<"foobar">>,PublicKeyEve,PrivateKeyAlice),
         PublicKeyAlice,PrivateKeyEve))
     ].
-endif.
