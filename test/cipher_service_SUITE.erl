%% @author alain.duranton
%% @doc @todo Add description to cipher_service_SUIT.


-module(cipher_service_SUITE).
-include_lib("common_test/include/ct.hrl").
-export([all/0]).
-export([alice_to_bob/1, init_per_testcase/2, end_per_testcase/2]).

all() ->
    [alice_to_bob].

init_per_testcase(alice_to_bob,Config) ->
    {ok, Pid}=gen_server:start_link({local, cipher_service}, cipher_service, [], []),
    [{pid, Pid}|Config].
end_per_testcase(alice_to_bob,Config) ->
    gen_server:stop(?config(pid, Config)).

alice_to_bob(_Config) ->
    {PublicKeyAlice, PrivateKeyAlice} = cipher_service:generate_key_pair(),
    {PublicKeyBob, PrivateKeyBob} = cipher_service:generate_key_pair(),
    {PublicKeyEve, PrivateKeyEve} = cipher_service:generate_key_pair(),
    PlainText = <<"It's a message from Alice to Bob, Eve must not be able to read it">>,
    CipherMessage = cipher_service:cipher(PlainText, PublicKeyBob, PrivateKeyAlice),
    PlainText = cipher_service:decipher(CipherMessage, PublicKeyAlice, PrivateKeyBob),
    <<>> = cipher_service:decipher(CipherMessage, PublicKeyAlice, PrivateKeyEve),
    FakeText = <<"Hi, this is almost Alice, are we good?">>,
    CipherFake = cipher_service:cipher(FakeText, PublicKeyBob, PrivateKeyEve),
    <<>> = cipher_service:decipher(CipherFake, PublicKeyAlice, PrivateKeyBob).