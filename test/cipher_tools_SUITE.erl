%% @author alain.duranton
%% @doc @todo Add description to cipher_tools_SUITE.


-module(cipher_tools_SUITE).

-include_lib("common_test/include/ct.hrl").
-export([all/0]).
-export([alice_to_bob/1]).

all() ->
    [alice_to_bob].

alice_to_bob(_Config) ->
    {PublicKeyAlice, PrivateKeyAlice} = cipher_tools:generate_key_pair(),
    {PublicKeyBob, PrivateKeyBob} = cipher_tools:generate_key_pair(),
    {PublicKeyEve, PrivateKeyEve} = cipher_tools:generate_key_pair(),
    PlainText = <<"It's a message from Alice to Bob, Eve must not be able to read it">>,
    CipherMessage = cipher_tools:cipher(PlainText, PublicKeyBob, PrivateKeyAlice),
    PlainText = cipher_tools:decipher(CipherMessage, PublicKeyAlice, PrivateKeyBob),
    <<>> = cipher_tools:decipher(CipherMessage, PublicKeyAlice, PrivateKeyEve),
    FakeText = <<"Hi, this is almost Alice, are we good?">>,
    CipherFake = cipher_tools:cipher(FakeText, PublicKeyBob, PrivateKeyEve),
    <<>> = cipher_tools:decipher(CipherFake, PublicKeyAlice, PrivateKeyBob).

