%% @author alain.duranton
%% @doc Easy to use 


-module(cipher_tools).

-define(CURVE, brainpoolP256t1).

%% ====================================================================
%% API functions
%% ====================================================================
-export([cipher/3, decipher/3, generate_key_pair/0]).


%% @doc cipher/3
%% Generate a key pair {PublicKey, PrivateKey} for
%% {ecdh,?CURVE}
%% ====================================================================
-spec generate_key_pair() -> Result when
          Result     :: {PublicKey, PrivateKey},
          PublicKey  :: binary(),
          PrivateKey :: binary().
%% ====================================================================
generate_key_pair() ->
    crypto:generate_key(ecdh,?CURVE).

%% @doc cipher/3
%% Encrypts and signs a <em>PlainText</em> with the recipient's public
%% key <em>PublicKey</em> and private key <em>PrivateKey</em> of the
%% issuer for {ecdh,?CURVE}
%% ====================================================================
-spec cipher(PlainText, PublicKey, PrivateKey) -> Result when
          PlainText  :: binary(),
          PublicKey  :: binary(),
          PrivateKey :: binary(),
          Result     :: binary().
%% ====================================================================
cipher(PlainText, PublicKey, PrivateKey) ->
    {EphemeralPublicKey, EphemeralPrivateKey} = generate_key_pair(),
    Ivec = crypto:strong_rand_bytes(16),
    Key = crypto:strong_rand_bytes(16),
    CipheredKey =
        crypto:exor(get_shared_secret(PublicKey,EphemeralPrivateKey),Key),
    Hash = crypto:hash(sha256, PlainText),
    CipherText = crypto:block_encrypt(
                   aes_cbc128,
                   Key,
                   Ivec,
                   pkcs7_padding_add(<<Hash/binary,PlainText/binary>>)),
    Sign = crypto:sign(ecdsa,
                       sha256,
                       EphemeralPublicKey,
                       [PrivateKey,?CURVE]),
    SignLength = length(binary_to_list(Sign)),
    EPKLength = length(binary_to_list(EphemeralPublicKey)),
    <<EPKLength:8,
      EphemeralPublicKey/binary,
      SignLength:8,
      Sign/binary,
      CipheredKey/binary,
      Ivec/binary,
      CipherText/binary>>.

%% @doc decipher/3
%% Verifiy and decrypt <em>CipheredTextWithHeaders</em> with the public
%% key <em>PublicKey</em> of the issuer and the private key
%% <em>PrivateKey</em> of the recipient for {ecdh,?CURVE}
%% ====================================================================
-spec decipher(CipheredTextWithHeaders, PublicKey, PrivateKey) -> Result
     when CipheredTextWithHeaders :: binary(),
          PublicKey               :: binary(),
          PrivateKey              :: binary(),
          Result                  :: binary().
%% ====================================================================
decipher(<<EPKLength:8,Part1/binary>>, PublicKey, PrivateKey) ->
    <<EphemeralPublicKey:EPKLength/binary,
      SignLength:8,
      Part2/binary>> = Part1,
    <<Sign:SignLength/binary,
      CipheredKey:16/binary,
      Ivec:16/binary,
      CipherText/binary>> = Part2,
    Key = crypto:exor(get_shared_secret(EphemeralPublicKey,PrivateKey),
                      CipheredKey),
    io:format("~w~n",[Sign]),
    case crypto:verify(ecdsa,
                       sha256,
                       EphemeralPublicKey,
                       Sign,[PublicKey,?CURVE]) of
        true ->
            <<Hash:32/binary,PaddedText/binary>> = 
                crypto:block_decrypt(aes_cbc128, Key, Ivec, CipherText),
            PlainText = pkcs7_padding_remove(PaddedText),
            case Hash =:= crypto:hash(sha256, PlainText) of
                true -> PlainText;
                _ -> <<>>
            end;
        _ -> <<>>
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% @doc pkcs7_padding_add/1
%% Complete <em>Binary</em> with padding in accordance with PKCS#7 for
%% an AES128 CBC block size
%% 
%% @reference <a href="http://tools.ietf.org/html/rfc5652#section-6.3">padding PKCS#7</a>
%% ====================================================================
-spec pkcs7_padding_add(Binary) -> Result when
          Binary :: binary(),
          Result :: binary().
%% ====================================================================
pkcs7_padding_add(Binary) -> 
    PadSize = 16-(size(Binary) rem 16),
    Pad = list_to_binary([PadSize||_<-lists:seq(1, PadSize)]),
    <<Binary/binary, Pad/binary>>.

%% @private
%% @doc pkcs7_padding_remove/1
%% Remove <em>Binary</em> padding in accordance with PKCS#7 for an
%% AES128 CBC block size.
%% On error, return an empty binary.
%% 
%% @reference <a href="http://tools.ietf.org/html/rfc5652#section-6.3">padding PKCS#7</a>
%% ====================================================================
-spec pkcs7_padding_remove(Binary) -> Result when
          Binary :: binary(),
          Result :: binary().
%% ====================================================================
pkcs7_padding_remove(Binary) ->
    Len = length(binary_to_list(Binary)),
    BitLenMinLast = Len-1,
    <<_:BitLenMinLast/binary,PadSize:8>> = Binary,
    Pad = list_to_binary([PadSize||_<-lists:seq(1, PadSize)]),
    case Len - PadSize of
        ResultLen when PadSize =< 16 ->
            <<Result:ResultLen/binary, Padding/binary>> = Binary,
            case Padding =:= Pad of
                true -> Result;
                _ -> <<>>
            end;
        _ -> <<>>
    end.

%% @private
%% @doc get_shared_secret/2
%% Get the {ecdh,?CURVE} shared secret between
%% <em>PublicKey</em> and <em>PrivateKey</em> truncated to an AES128
%% key size
%% ====================================================================
-spec get_shared_secret(PublicKey,PrivateKey) -> Result when
          PublicKey  :: binary(),
          PrivateKey :: binary(),
          Result     :: binary().
%% ====================================================================
get_shared_secret(PublicKey,PrivateKey) ->
    <<Shared:16/binary,_/binary>> = 
        crypto:compute_key(ecdh,PublicKey,PrivateKey,?CURVE),
    Shared.
