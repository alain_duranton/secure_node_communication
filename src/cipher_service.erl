%% @author alain.duranton
%% @doc @todo Add description to cipher_service.


-module(cipher_service).
-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% API functions
%% ====================================================================
-export([cipher/3, decipher/3, generate_key_pair/0]).

%% @doc generate_key_pair/0
%% @see cipher_tools:generate_key_pair/0
%% ====================================================================
-spec generate_key_pair() -> Result when
          Result     :: {PublicKey, PrivateKey},
          PublicKey  :: binary(),
          PrivateKey :: binary().
%% ====================================================================
generate_key_pair() ->
    gen_server:call(?MODULE, {generate_key_pair}).

%% @doc cipher/3
%% @see cipher_tools:cipher/3
%% ====================================================================
-spec cipher(PlainText, PublicKey, PrivateKey) -> Result when
          PlainText  :: binary(),
          PublicKey  :: binary(),
          PrivateKey :: binary(),
          Result     :: binary().
%% ====================================================================
cipher(PlainText, PublicKey, PrivateKey) ->
    gen_server:call(?MODULE, {cipher, PlainText, PublicKey, PrivateKey}).
%% @doc decipher/3
%% @see cipher_tools:decipher/3
%% ====================================================================
-spec decipher(CipheredTextWithHeaders, PublicKey, PrivateKey) -> Result
     when CipheredTextWithHeaders :: binary(),
          PublicKey               :: binary(),
          PrivateKey              :: binary(),
          Result                  :: binary().
%% ====================================================================
decipher(CipheredTextWithHeaders, PublicKey, PrivateKey) ->
    gen_server:call(?MODULE, {decipher, CipheredTextWithHeaders, PublicKey, PrivateKey}).

%% ====================================================================
%% Behavioural functions
%% ====================================================================
-record(state, {}).

%% init/1
%% ====================================================================
%% @doc <a href="http://www.erlang.org/doc/man/gen_server.html#Module:init-1">gen_server:init/1</a>
-spec init(Args :: term()) -> Result when
	Result :: {ok, State}
			| {ok, State, Timeout}
			| {ok, State, hibernate}
			| {stop, Reason :: term()}
			| ignore,
	State :: term(),
	Timeout :: non_neg_integer() | infinity.
%% ====================================================================
init([]) ->
    {ok, #state{}}.


%% handle_call/3
%% ====================================================================
%% @doc <a href="http://www.erlang.org/doc/man/gen_server.html#Module:handle_call-3">gen_server:handle_call/3</a>
-spec handle_call(Request :: term(), From :: {pid(), Tag :: term()}, State :: term()) -> Result when
	Result :: {reply, Reply, NewState}
			| {reply, Reply, NewState, Timeout}
			| {reply, Reply, NewState, hibernate}
			| {noreply, NewState}
			| {noreply, NewState, Timeout}
			| {noreply, NewState, hibernate}
			| {stop, Reason, Reply, NewState}
			| {stop, Reason, NewState},
	Reply :: term(),
	NewState :: term(),
	Timeout :: non_neg_integer() | infinity,
	Reason :: term().
%% ====================================================================
handle_call({cipher, PlainText, PublicKey, PrivateKey}, _From, State) ->
    {reply, cipher_tools:cipher(PlainText, PublicKey, PrivateKey), State};
handle_call({decipher, CipheredTextWithHeaders, PublicKey, PrivateKey}, _From, State) ->
    {reply, cipher_tools:decipher(CipheredTextWithHeaders, PublicKey, PrivateKey), State};
handle_call({generate_key_pair}, _From, State) ->
    {reply, cipher_tools:generate_key_pair() , State};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% ====================================================================
%% default callbacks
%% ====================================================================
handle_cast(_Msg, State) -> {noreply, State}.
handle_info(_Info, State) -> {noreply, State}.
terminate(_Reason, _State) -> ok.
code_change(_OldVsn, State, _Extra) -> {ok, State}.
